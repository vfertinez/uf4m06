import java.sql.*;

public class DatabaseMysql extends Database {
    static void consultarSQL(){
        String uri ="jdbc:mysql://localhost/mydatabase?user=myuser&password=mypass";
        try(Connection conn = DriverManager.getConnection(uri)){
            ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM movies");
            while (resultSet.next()){
                System.out.println(resultSet.getString("title"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertarPeliculaSQL(String title) {
        String uri ="jdbc:mysql://localhost/mydatabase?user=myuser&password=mypass";
        try(Connection conn = DriverManager.getConnection(uri)){
            //INSERT
            PreparedStatement statement = conn.prepareStatement("INSERT INTO movies(title) VALUES(?)");
            statement.setString(1, "Batman");
            statement.executeUpdate();

            //QUERY
            ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM movies");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("title"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void consultarSQLConcreto(String title){
        String uri ="jdbc:mysql://localhost/mydatabase?user=myuser&password=mypass";
        try(Connection conn = DriverManager.getConnection(uri)){
            ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM movies where title = '"+title+"'");
            while (resultSet.next()){
                System.out.println(resultSet.getString("title"));

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
