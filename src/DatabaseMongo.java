import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import static com.mongodb.client.model.Filters.eq;

public class DatabaseMongo extends Database {
    public static void insertarPeliculaMongo(String title) {
        String uri = "mongodb://localhost";
        try (MongoClient mongoClient = MongoClients.create(uri)) {
            MongoDatabase database = mongoClient.getDatabase("sampledb");
            MongoCollection<Document> collection = database.getCollection("movies");

            // INSERT
            Document doc = new Document();
            doc.append("title", "Batman");
            collection.insertOne(doc);

            //QUERY
            System.out.println(collection.find(eq("title", "Batman")).first().toJson());
        }
    }
    static  void  consultarMongo(){
        String uri = "mongodb://localhost";
        try (MongoClient mongoClient = MongoClients.create(uri)) {
            MongoDatabase database = mongoClient.getDatabase("sampledb");
            MongoCollection<Document> collection = database.getCollection("movies");
            System.out.println(collection.find().first().toJson());

        }
    }
    public void consultarMongoConcreto(String title){
        String uri = "mongodb://localhost";
        try(MongoClient mongoClient = MongoClients.create(uri)){
            MongoDatabase database = mongoClient.getDatabase("sampledb");
            MongoCollection<Document> collection = database.getCollection("movies");
            System.out.println(collection.find(eq("title", title)).first().toJson());
        }

    }
}
